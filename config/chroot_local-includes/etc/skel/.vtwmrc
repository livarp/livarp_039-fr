########################################################################
#                 vtwm configuration file ~/.vtwmrc                    #
#             author: arpinux 2012 <contact@arpinux.org>               #
########################################################################

## polices #############################################################
TitleFont           "-*-snap-normal-*-*-*-10-*-*-*-*-*-*-*" # police de la barre de titre
ResizeFont          "-*-snap-normal-*-*-*-10-*-*-*-*-*-*-*" # police de la fenêtre d'infos de redimensionnement
MenuFont            "-*-snap-normal-*-*-*-10-*-*-*-*-*-*-*" # police des menus
MenuTitleFont       "-*-snap-normal-*-*-*-10-*-*-*-*-*-*-*" # police des titres de menus
IconFont            "-*-snap-normal-*-*-*-10-*-*-*-*-*-*-*" # police des icons
IconManagerFont     "-*-snap-normal-*-*-*-10-*-*-*-*-*-*-*" # police de la boite d'icones
# décommenter pour afficher le nom des fenêtres dans le pager
#VirtualDesktopFont  "-*-snap-normal-*-*-*-10-*-*-*-*-*-*-*"
## écran ###############################################################
AutoPan 100            # déplacer l'écran de 100% lorsque le pointeur atteint le bord
AutoPanBorderWidth 5   # distance pour enclencher le déplacement
NaturalAutopanBehavior # déplace le pointer au bords, pas à l'écran
SnapRealScreen         # étale l'écran sur la dimension du décallage (multi-moniteur)
PanDistanceY 100       # pourcentage de déplacement vertical
PanDistanceX 100       # pourcentage de déplacement horizontal
PanResistance 1000     # temps de déplacement en ms
## pager ###############################################################
VirtualDesktop "4x1-5+5" 30 # taille et position du pager
DontShowInDisplay {
    "VTWM Icon Manager"	"VTWM Desktop" "xclock"
}                           # applications à ne pas afficher dans le pager
## bordures ############################################################
BorderWidth 1               # bordures
IconBorderWidth 1           # bordures des icones
TitleButtonBorderWidth 0    # bordures des boutons de la barre de titre
## boite d'icones ######################################################
ShowIconManager                            # afficher la boite à icones au démarrage
IconManagerGeometry "100x700+5+95" 1       # taille, géométrie et nombre de colonnes de la boite à icones
#IconManagerBevelWidth 1                    # relief dans la boite à icones
IconManagerDontShow { "xclock" "xosview" } # ne pas afficher dans la boite à icones
IconifyByUnmapping                         # ne pas afficher les icones
## fenêtres ############################################################
MaxWindowSize "1010x760"   # taille maximale des fenêtres
RestartPreviousState       # tente de restaurer l agéométrie des clients
Zoom 8                     # facteur d'animation zoom
WindowRing                 # prendre en charge toutes les fenêtres
AutoRelativeResize         # ne pas attendre d'atteindre un bord pour lancer le redimensionnement
#AutoRaise                  # passe au premier plan automatiquement
#AutoRaiseDelay 1000        # temps de passage au premier plan en ms
DeiconifyToScreen          # restaure les clients iconifiés dans le bureau courant
WarpCursor                 # place le pointeur sur la fenêtre restaurée
NoRaiseOnMove              # ne pas passer au premier plan lors d'un déplacement
NailedDown {
    "VTWM Icon Manager" "VTWM Desktop" "xclock"
}                          # afficher sur tous les bureaux
DontShowInVtwmWindows {
    "VTWM Icon Manager"	"VTWM Desktop" "xclock"
}                          # ne pas afficher dans le menu fenêtres
## barre de titre ######################################################
SqueezeTitle {"URxvt" right 0 0}       # reduit la taile de la barre de titre et la place à droite
NoTitle {
    "xclock""xosview""MPlayer""VTWM *"
}                                      # clients sans barre de titre
ButtonIndent 4                         # facteur de réduction des boutons
FramePadding 0                         # marges entre les éléments de la barre de titre
TitlePadding 4                         # marges de la barre de titre
## boutons #############################################################
# répertoires des icones
IconDirectory "/usr/share/dzicons"
NoDefaultTitleButtons            # ne prends pas en compte les icones par défaut
# set buttons on titlebars
LeftTitleButton "error1.xbm"     = f.delete
LeftTitleButton "downarrow4.xbm" = f.iconify
LeftTitleButton "stop.xbm"       = f.nail
RightTitleButton "monocle.xbm"   = f.fullzoom
RightTitleButton "debian.xbm"    = f.resize
## highlight ###########################################################
NoHighlight         # pas de mise en évidence
NoTitleHighlight    # pas de mise en évidence sur la barre de titre
## divers ##############################################################
NoGrabServer              # ne pas utiliser le serveur X pour les déplacement

## couleurs ############################################################
Color {
    BorderColor              "gray70"
    BorderTileBackground     "gray25"
	BorderTileForeground     "gray85"
    DefaultBackground        "gray25"
    DefaultForeground        "gray85"
    TitleBackground          "gray25"
    TitleForeground          "gray85"
    MenuBackground           "gray25"
    MenuForeground           "gray85"
    MenuTitleBackground      "gray95"
    MenuTitleForeground      "gray25"
    MenuShadowColor          "gray55"
    IconBackground           "gray25"
    IconForeground           "gray65"
    IconBorderColor          "gray85"
    IconManagerBackground    "gray25"
    IconManagerForeground    "gray85"
    IconManagerHighlight     "gray70"
    VirtualBackground        "gray25"
	VirtualForeground        "gray85"
	DesktopDisplayBackground "gray85"
    DesktopDisplayForeground "gray25"
    DesktopDisplayBorder     "gray25"
}

## actions de la souris ################################################
# combien de pixels avant de bouger
MoveDelta 1
# fonctions
Function "move-or-lower"   { f.move f.deltastop f.lower }
Function "move-or-raise"   { f.move f.deltastop f.raise }
Function "move-or-iconify" { f.move f.deltastop f.iconify }
Function "resize-or-raise" { f.resize f.deltastop f.raise }
# actions de la souris sur le bureau
Button1 = : root : f.menu "main"    # open main menu on left-clic
Button2 = : root : f.identify       # identify window on middle-clic
Button3 = : root : ! "compiz-deskmenu &"  # open deskmenu on right-clic
Button4 = : root : f.panright "100" # pan screen 100% to the right
Button5 = : root : f.panleft  "100" # pan screen 100% to the left
# acions de la souris sur la fenêtre ou l'icone
Button1 = m : window|icon : f.function "move-or-lower"   # move/lower with Alt+left-clic
Button2 = m : window|icon : f.iconify                    # toggle iconify with Alt+middle-clic
Button3 = m : window|icon : f.function "resize-or-raise" # resize/raise with Alt+right-clic
# action de la souris sur la barre de titre
Button1 = : title : f.function "move-or-raise"  # move/raise on left-clic
Button2 = : title : f.raiselower                # toggle raise on middle-clic
Button3 = : title : f.menu "window"             # open window menu on right-clic
# action de la souris sur l'icone
Button1 = : icon : f.function "move-or-iconify" # move/iconify on left-clic
Button2 = : icon : f.delete                     # close window on middle-clic
Button3 = : icon : f.iconify                    # toggle iconify on right-clic
# action de la souris sur la boite à icones
Button1 = : iconmgr : f.iconify # toggle iconify on left-clic
Button2 = : iconmgr : f.destroy # kill the window on middle-clic
Button3 = : iconmgr : f.iconify # toggle iconify on right-clic

## raccoucis clavier ###################################################
# lancer des applications
"Return" = c : all :  ! "urxvtc &"         # Ctrl+Enter: terminal par défaut
"d" = m4 : all : ! "dmenu-bind.sh &"       # Super+d: lancer dmenu
"t" = m4 : all : ! "urxvtc &"              # Super+t: terminal
"r" = m4 : all : ! "urxvtc -e ranger &"    # Super+r: ranger
"r" = s|m4 : all : ! "rox &"               # Super+Shift+r: rox-filer
"w" = m4 : all : ! "firefox &"             # Super+w: firefox
"v" = m4 : all : ! "urxvtc -e alsamixer &" # Super+v: controleur de volume
# navigation
"Tab" = m : all :   f.warpring "next" # client suivant: Alt+Tab
"Tab" = s|m : all : f.warpring "prev" # client précédent: Alt+Shift+Tab
"Right" = c : all : f.panright "100" # bureau de droite: Ctrl+droite
"Left"  = c : all : f.panleft  "100" # bureau de gauche: Ctrl+gauche
"Up"    = c : all : f.panup    "100"
"Down"  = c : all : f.pandown  "100"
# actions sur les fenêtres
"m" = m4 : all : f.fullzoom # Super+m: client maximisé oui/non
"i" = m4 : all : f.iconify  # Super+i: client iconifié oui/non
"q" = m4 : all : f.delete   # Super+q: fermer le client
"x" = m4 : all : f.destroy  # Super+x: tuer le client
# actions vtwm
"r" = c|m : all : f.restart                # Control+Alt+r relancer vtwm
"q" = c|s : all : f.quit                   # Control+Shift+q quitter vtwm
"q" = c|s|m : all : f.exec "dmenu-quit.sh" # Control+Shift+Alt+q quitter livarp
## menus ###############################################################
# menu principal
menu "main" {
    "VTWM"      ("gray95":"gray25")f.title
    "URxvt"     f.exec "urxvtc &"
    "Ranger"    f.exec "urxvtc -e ranger &"
    "Lancer"    f.exec "dmenu-bind.sh &"
    "Editer"    f.exec "urxvtc -e vim &"
    "Internet"  f.exec "firefox &"
    "IRC"       f.exec "urxvtc -e weechat-curses &"
    "Aide"      f.exec "luakit /usr/share/livarp/help_center/index-fr.html &"
    "Config"    f.menu "config"
    "Système"   f.menu "system"
    "Fenêtres"  f.menu "window"
    "Quitter"   ("red":"gray25")f.menu "quit"
}
# config menu
menu "config" {
    "VTWM config" ("green":"gray25")f.title
    "Editer"      f.exec "urxvtc -e vim .vtwmrc &"
    "Relancer"    f.restart
    "Startup"     f.exec "urxvtc -e vim bin/start/vtwm_start.sh &"
    "Raccourcis"  f.exec "urxvtc --geometry 60x30+50+50 -e vtwm_keys.sh &"
    "Conky"       f.exec "urxvtc -e vim .conky/.conkyrc_vtwm &"
    "Fenêtres"    f.menu "window"
    "Manuel"      f.exec "urxvtc -e man vtwm &"
}
# system menu
menu "system" {
    "System" 	("blue":"gray25")f.title
    "Xosview"	f.exec "xosview &"
    "HTop"		f.exec "urxvtc -e htop &"
    "Aptitude"  f.exec "urxvtc -e aptitude &"
}
# window menu
menu "window" {
    "X Windows"       ("yellow":"gray25")f.title
    "Titre"           f.menu "title"
    "Position"        f.menu "position"
    "Navigation"      f.menu "navigation"
    "Maximiser"       f.fullzoom
    "Iconifier"       f.iconify
    "Fixer(o/n)"      f.nail
    "Redimensionner"  f.resize
    "Identifier"      f.identify
    "Fermer"          f.delete
    "Forcer Fermer"   ("red":"gray25")f.destroy
    ""                ("gray25":"gray25")f.nop
    "Twm"             ("LightGreen":"gray25")f.title
    "Cacher IconBox"  f.hideiconmgr
    "Montrer IconBox" f.showiconmgr
    "Cacher Pager"    f.hidedesktopdisplay
    "Montrer Pager"   f.showdesktopdisplay
    "Vue pécédente"   f.panleft "100"
    "Vue suivante"    f.panright "100"
}
# title menu
menu "title" {
    "à Gauche"  f.squeezeleft
    "Centré"    f.squeezecenter
    "à Droite"  f.squeezeright
}
# position menu
menu "position" {
    "Placer à Droite"  f.rightzoom 
    "Placer à Gauche"  f.leftzoom
    "Placer au Centre" f.horizoom
    "Placer en Haut"   f.topzoom
    "Placer en Bas"    f.bottomzoom
}
# navigation menu
menu "navigation" {
    "Client Précédent" f.warpring "prev"
    "Client Suivant"   f.warpring "next"
}
# exit menu
menu "quit" {
    "Quitter?" ("red":"gray25")f.title
    "Non"      f.nop
    "Oui"      f.quit
}
## cursors #############################################################
Cursors {
    Frame     "left_ptr"
    Title     "left_ptr"
    Icon      "left_ptr"
    IconMgr   "left_ptr"
    Move      "fleur"
    Resize    "fleur"
    Menu      "hand1"
    Button    "hand2"
    Wait      "clock"
    Select    "dot"
    Destroy   "pirate"
}
## EOF #################################################################
