-- dossiers
wallpapers = "/usr/share/backgrounds"
screenshots = home .. "/tmp/"

-- police à chasse fixe pour les popups
monofont = "DroidSansMono 7"

-- menu awesome
menu_icon = true   -- si true, affiche l'icone de menu dans la barre. le menu reste accessible via un clic-droit sur le bureau ou Alt+p
menu_lang = "fr"   -- fr ou en

--taglists
taglist = "static"        -- static ou dynamic
-- si taglist = "static", définitions de l'affichage des tags:
taglist_style = "awesome" -- options: awesome (a,w,e,s,o,m,e...), arabic (1,2,3...),east_arabic (١, ٢, ٣,...), persian_arabic(٠,١,٢,٣,۴,....}, roman (I, II, III, IV,)

--clients
enable_titlebar = false       -- afficher la barre de titre des clients
enable_floatbar = false       -- afficher la barre de titre des clients flottants
tasklist_icon_enable = false  -- afficher les icones dans la liste des clients

--wibox
widget_mode = "graph"      -- type des widgets: text/graph
show_icons = true          -- afficher les icones: true/false -- si les icones ne sont pas dans le thème, utilisation des icones par défaut (voir plus bas)
top_bar_visible = true     -- afficher la barre supérieure: true/false -- affiche/masque avec ctrl+alt+t -- seulement sur l'écran principal
bottom_bar_visible = false -- afficher la barre inférieure: true/false -- affiche/masque avec ctrl+alt+b -- seulement sur l'écran principal
invert_bar = false         -- inverser les barres (top is bottom and bottom is top)
wibox_opacity = 0.85       -- opacité des wibox (tranparence entre 0 et 1)

--date widget
date_lang = "fr_FR.UTF-8"      -- en_EN.UTF-8 pour english -- fr_FR.UTF-8 pour français
date_format = "%a %d %b %Y %R" -- selon http://en.wikipedia.org/wiki/Date_(Unix)

--cpu widget
cpu_enable = true      -- affiche les infos CPU
cputemp_enable = false -- affiche la température CPU

--mem widget
mem_enable = true             -- affiche les infos mémoire
memtext_enable = false        -- affiche le texte -- 
memtext_format = " $2Mb utilisé" -- format du texte: $1 pourcentage, $2 utilisé $3 total $4 libre

--disk widget
diskbootbar_enable = false  -- afficher la partition /boot
diskrootbar_enable = true   -- afficher la partiton /
diskhomebar_enable = false  -- afficher la partition /home

--system
uptime_enable = true        -- afficher le temps de connexion et la charge système

--network widget
net_enable = true           -- afficher le traffic réseau
apt_enable = false          -- afficher les mises à jour de Apt si nécessaire
gmail_enable = false        -- afficher les mails non-lus (renseigner ~/.netrc avec -> machine mail.google.com login <e-mail address> password <password>)
weather_enable = false      -- afficher les infos météo
weather_code = "LFPO"       -- code à appliquer pour la météo

--volume widget
vol_enable = true           -- afficher le volume

-- music widget
mpd_enable = false          -- afficher les infos mpd (dépend de mpd et mpc)
moc_enable = true           -- afficher les infos moc

--bat widget
battery_enable = true       -- afficher les infos de la batterie

--info help icon
infoicon_enable = true      -- afficher le bouton d'aide

--icones par défaut si non présente dans le thème sélectionné.
default_cal_img = os.getenv("HOME").."/.config/awesome/icons/dzen/clock.png"
default_cpu_img = os.getenv("HOME").."/.config/awesome/icons/dzen/cpu.png"
default_mem_img = os.getenv("HOME").."/.config/awesome/icons/dzen/mem.png"
default_bat_img = os.getenv("HOME").."/.config/awesome/icons/dzen/bat_full_01.png"
default_fs_img = os.getenv("HOME").."/.config/awesome/icons/dzen/fs_01.png"
default_info_img = os.getenv("HOME").."/.config/awesome/icons/dzen/info_01.png"
default_vol_img = os.getenv("HOME").."/.config/awesome/icons/dzen/spkr_01.png"
default_music_img = os.getenv("HOME").."/.config/awesome/icons/dzen/note.png"
default_netup_img = os.getenv("HOME").."/.config/awesome/icons/dzen/net_up_01.png"
default_netdn_img = os.getenv("HOME").."/.config/awesome/icons/dzen/net_down_01.png"




