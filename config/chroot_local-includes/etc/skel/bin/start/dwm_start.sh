#! /bin/bash
# livarp_0.3.9 dwm start-up script
##################################

## script de post-install ## -------------------------------------------
## peut être effacer après exécution
if ! [ -d /home/user ]; then
    if ! [ -e /usr/share/livarp/livarpfirstlog ]; then
        (sleep 10s && urxvtc -e su-to-root -c /usr/local/bin/livarp_postinstall.sh) &
    fi
fi

## lancer le client mail ## --------------------------------------------
#if ping -c 1 -w 1 194.2.0.20 &>/dev/null; then
#    sleep 5 && claws-mail &
#fi

## modifier la touche caps_lock en Super - pour les vieux portables ----
#xmodmap ~/.Xmodmap

## lancer les effets composite -----------------------------------------
#xcompmgr -fF &
#xcompmgr -cCfF -I20 -O10 -D1 -t-5 -l-5 -r4.2 -o.82

## lancer fbpanel ------------------------------------------------------
sleep 1 && fbpanel -p dwm &

## montage automatique -------------------------------------------------
sleep 10 && udisks-glue --session &

## fond d'écran dwm ----------------------------------------------------
#nitrogen --restore ## décommenter pour afficher votre fond d'écran
feh --no-xinerama --bg-fill /usr/share/backgrounds/livarp.png

## définir la barre de status ------------------------------------------
conkyd -q -c ~/.conky/.conkyrc_dwm | while true; read line; do xsetroot -name "$line"; done &

## connexion internet --------------------------------------------------
nm-applet &

## dock minimal --------------------------------------------------------
#sleep 10s && ~/bin/tabdock.sh &

## lancer dwm avec un fichier de logs ----------------------------------
~/.config/livarp-start.sh &
while true; do
    dwm 2> ~/.dwm.log
done
