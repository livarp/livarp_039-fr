#! /bin/bash
# livarp_0.3.9 compiz start-up script
#####################################

## script de post-install ## -------------------------------------------
## peut être effacer après exécution
if ! [ -d /home/user ]; then
    if ! [ -e /usr/share/livarp/livarpfirstlog ]; then
        (sleep 10s && urxvtc -e su-to-root -c /usr/local/bin/livarp_postinstall.sh) &
    fi
fi

## lancer le client mail ## --------------------------------------------
#if ping -c 1 -w 1 194.2.0.20 &>/dev/null; then
#    sleep 5 && claws-mail &
#fi

## modifier la touche caps_lock en Super - pour les vieux portables ----
#xmodmap ~/.Xmodmap

## lancer fbpanel ------------------------------------------------------
sleep 5 && fbpanel -p compiz &

## montage automatique -------------------------------------------------
sleep 10 && udisks-glue --session &

## fond d'écran compiz -------------------------------------------------
#nitrogen --restore ## décommenter pour afficher votre fond d'écran
feh --no-xinerama --bg-fill /usr/share/backgrounds/livarp_0.3.9_compiz.png

## lancer conky --------------------------------------------------------
sleep 3 && conky -q -c ~/.conky/.conkyrc_compiz &

## connexion internet --------------------------------------------------
nm-applet &

## dock minimal --------------------------------------------------------
#sleep 10s && ~/bin/tabdock.sh &

## lancer compiz -------------------------------------------------------
~/.config/livarp-start.sh &
exec compiz ccp
