#! /bin/bash
# arpinux awesome start-up script
#################################

## script de post-install ## -------------------------------------------
## peut être effacer après exécution
if ! [ -d /home/user ]; then
    if ! [ -e /usr/share/livarp/livarpfirstlog ]; then
        (sleep 10s && urxvtc -e su-to-root -c /usr/local/bin/livarp_postinstall.sh) &
    fi
fi

## lancer le client mail ## --------------------------------------------
#if ping -c 1 -w 1 194.2.0.20 &>/dev/null; then
#    sleep 5 && claws-mail &
#fi

## modifier la touche caps_lock en Super - pour les vieux portables ----
#xmodmap ~/.Xmodmap

## lancer les effets composite -----------------------------------------
#xcompmgr -fF &
#xcompmgr -cCfF -I20 -O10 -D1 -t-5 -l-5 -r4.2 -o.82

## montage automatique -------------------------------------------------
sleep 10 && udisks-glue --config .udisk-glue-awesome.conf --session &

## fond d'écran awesome ------------------------------------------------
feh --no-xinerama --bg-fill .config/awesome/current_wallpaper

## lancer conky --------------------------------------------------------
conky -q -c ~/.conky/.conkyrc_awesome &

## connexion internet --------------------------------------------------
nm-applet &

## dock minimal --------------------------------------------------------
#sleep 10s && ~/bin/tabdock.sh &

## lancer awesome ------------------------------------------------------
~/.config/livarp-start.sh &
exec awesome
