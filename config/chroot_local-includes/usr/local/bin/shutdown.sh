#!/bin/sh

ACTION=`zenity --width=90 --height=255 --list --radiolist --text="Sélectionner une action" --title="Logout" --column "Choice" --column "Action" TRUE Eteindre FALSE Redemarrer FALSE Suspendre FALSE Hiberner FALSE Verrouiller FALSE Déconnecter`

if [ -n "${ACTION}" ];then
  case $ACTION in
  Eteindre)
    dbus-send --system --print-reply --dest="org.freedesktop.ConsoleKit" /org/freedesktop/ConsoleKit/Manager org.freedesktop.ConsoleKit.Manager.Stop
    ;;
  Redemarrer)
    dbus-send --system --print-reply --dest="org.freedesktop.ConsoleKit" /org/freedesktop/ConsoleKit/Manager org.freedesktop.ConsoleKit.Manager.Restart
    ;;
  Verrouiller)
    xscreensaver-command -lock
    ;;
  Suspendre)
   dbus-send --system --print-reply --dest="org.freedesktop.UPower" /org/freedesktop/UPower org.freedesktop.UPower.Suspend
   ;;
  Hiberner)
   dbus-send --system --print-reply --dest="org.freedesktop.UPower" /org/freedesktop/UPower org.freedesktop.UPower.Hibernate
   ;;
  Déconnecter)
   xdotool key Ctrl+Alt+BackSpace
   ;;
  esac
fi
