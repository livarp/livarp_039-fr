#! /bin/bash
# livarp_0.3.9 echinus keybinds & shortcuts
###########################################

## text colors ---------------------------------------------------------
red='\e[0;31m'
blue='\e[0;34m'
cyan='\e[0;36m'
green='\e[0;32m'
yellow='\e[0;33m'
## No Color
NC='\e[0m'
## background colors ---------------------------------------------------
RED='\e[41m'
BLUE='\e[44m'
CYAN='\e[46m'
GREEN='\e[42m'
YELLOW='\e[43m'
## output --------------------------------------------------------------
echo ""
echo -e "     ${green}raccourcis clavier echinus-livarp 0.3.9$NC"
echo ""
echo -e " ${RED} relancer echinus   $NC Ctrl + Alt + r"
echo -e " ${RED} quitter echinus    $NC Ctrl + Alt + Backspace"
echo -e " ${RED} quitter livarp     $NC Ctrl + Shift + Alt + q"
echo -e " ${RED} fermer le client   $NC Super + q"
echo ""
echo -e " ${BLUE} lancer dmenu         $NC Alt + d"
echo -e " ${GREEN} ouvrir a terminal    $NC Super + Return"
echo -e " ${GREEN} ouvrir rox-filer     $NC Alt + Shift + r"
echo -e " ${GREEN} ouvrir ranger        $NC Alt + r"
echo -e " ${GREEN} ouvrir luakit        $NC Alt + w"
echo -e " ${GREEN} ouvrir firefox       $NC Alt + Shift + w"
echo -e " ${GREEN} ouvrir vim editor    $NC Alt + e"
echo -e " ${GREEN} ouvrir geany         $NC Alt + Shift + e"
echo -e " ${GREEN} ouvrir weechat       $NC Alt + x"
echo -e " ${GREEN} ouvrir music player  $NC Alt + z"
echo -e " ${GREEN} controler le volume  $NC Alt + v"
echo ""
echo -e " ${BLUE} agencement tile         $NC Alt + t"
echo -e " ${BLUE} agencement bottomstack  $NC Alt + s"
echo -e " ${BLUE} agencement maximisé     $NC Alt + m"
echo -e " ${BLUE} agencement ifloating    $NC Alt + i"
echo -e " ${BLUE} agencement floating     $NC Alt + f"
echo -e " ${BLUE} libère le client        $NC Alt + espace"
echo ""
echo -e " ${CYAN} client prec/suivant     $NC Alt + k/j"
echo -e " ${CYAN} tag prec/suivant        $NC Ctrl + Gauche/Droite"
echo -e " ${CYAN} écran prec/suivant      $NC Alt + Haut"
echo ""
echo -e " ${CYAN} envoi au tag 'n'        $NC Alt + Shift + [F1..Fn]"
echo -e " ${CYAN} affiche le tag 'n'      $NC Alt + [F1..Fn]"
echo ""
echo ""
echo -e " ${green} manuel complet echinus >> [man echinus]"
echo -en " ${green} presser Enter pour quitter..."
read anykey
exit 0
