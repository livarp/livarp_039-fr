#!/bin/sh
#############################################
## script de post-installation livarp0.3.9 ##
##-----------------------------------------##
clear
# are you root ?
# ----------------------------------------------------------------------
if [ "`whoami`" != "root" ]; then
    echo ""
    echo " vous n'êtes pas l'administrateur..."
    echo " ce script a besoin des droits administrateurs"
    echo "   veuillez lancer ce script en 'root'."
    sleep 4s && exit 0
fi
# set user
# ----------------------------------------------------------------------
HUMAN=`w -h | tail -n1 | awk '{print $1}'`
# allow user to sudo halt/reboot without password
# ----------------------------------------------------------------------
if [ "`cat /etc/sudoers | grep shutdown`" = "" ]; then
    echo "$HUMAN    ALL=(ALL) NOPASSWD: /sbin/shutdown, /sbin/reboot" >> /etc/sudoers
fi
# fix network-manager
# ----------------------------------------------------------------------
sed --in-place=.bak 's/managed=false/managed=true/' /etc/NetworkManager/NetworkManager.conf
# ask for confirmation
# ----------------------------------------------------------------------
echo ""
echo " ################################################################"
echo " # bienvenue dans le script de post-installation du livarp0.3.9 #"
echo " #             ce script ne dure que quelques minutes.          #"
echo " ##------------------------------------------------------------##"
echo ""
echo ""
echo "  ce script termine l'installation du livarp_0.3.9:"
echo "     - purge des paquets de construction du livecd"
echo "     - ajout des archives main/contrib/non-free"
echo "     - ajout des dépôts testing, instable et multimedia"
echo "     - ajout du fichiers apt/preferences pour le pinning"
echo "       Debian Squeeze reste prioritaire"
echo "     - mise à jour des dépôts et du système"
echo "     - possibilité d'ajouter pidgin,"
echo "       le client de messagerie multi-protocole."
echo ""
echo ""
echo " vous aurez besoin d'une connexion internet active"
echo " vous pouvez exécuter ce script plus tard.. presser Enter pour continuer."
echo -n " (O|n) > "
read a
if [ "$a" = "n" ] || [ "$a" = "N" ]; then
    echo ""
    echo " ce script se lancera au prochain login"
    echo ""
    sleep 4s && exit 0
fi
# purge des paquets live*
# ----------------------------------------------------------------------
echo ""
echo -n " desinstaller les paquets de construction du live ? (O|n)"
read a
if [ "$a" = "o" ] || [ "$a" = "O" ] || [ "$a" = "" ]; then
    apt-get autoremove --purge -y --force-yes live-boot live-boot-initramfs-tools live-config live-config-sysvinit live-initramfs
fi
# apt-setup
# ----------------------------------------------------------------------
echo " Test de la connexion internet..."
sleep 2s
echo ""
IS=`/bin/ping -c 1 ftp.de.debian.org | grep -c "64 bytes"`
if [ "$IS" -lt "1" ]; then
    until [ "$CONT" != "" ]; do
    echo ""
    IS=`/bin/ping -c 1 ftp.de.debian.org | grep -c "64 bytes"`
    if [ "$IS" -lt "1" ]; then
        clear
        echo "  Echec au test de la connexion internet."
        echo ""
        echo "  Configurez votre connexion internet"
        echo "  puis pressez une touche pour continuer, ou \"q + Enter\""
        echo "  pour quitter ce script script."
        read a
        if [ "$a" = "q" ]; then
            clear
            echo " on se revoit au prochain login. "
            echo ""
            exit 0
        fi
        else
            CONT="pass"
        fi
    done
fi
clear
echo "  connexion testée avec succès..."
echo ""
if ! [ -e /etc/apt/sources.list.bak ]; then
    echo ""
    echo " configuration de \"apt\":"
    echo " - ajout des dépôts stable/testing/instable ~ main/contrib/non-free."
    echo " - ajout du fichier /etc/apt/preferences pour le \"pinning\"."
    echo ""
    echo " >> Debian Squeeze reste prioritaire <<"
    echo ""
    echo ""
    echo -n "presser Enter pour continuer "
    read anykey
    echo ""
    echo " ajout du sources.list et du fichier preferences."
    echo ""
    mv /etc/apt/sources.list /etc/apt/sources.list.bak
    cp /usr/share/livarp/sources.list /etc/apt/sources.list
    sleep 3s
fi
# apt-update
# ----------------------------------------------------------------------
echo ""
echo " mise à jour des dépots et installation du trousseau multimedia."
echo ""
sleep 3s
apt-get update && apt-get install --allow-unauthenticated deb-multimedia-keyring
echo ""
echo " mise à jour des dépôts et du système."
echo ""
sleep 3s
apt-get update
apt-get dist-upgrade -y --force-yes
echo ""
touch /usr/share/livarp/livarpfirstlog
echo "fichier auto-généré lors de la post-installation. ne pas effacer." > /usr/share/livarp/livarpfirstlog
echo ""
echo " votre système est à jour."
echo ""
echo " vous avez weechat & mcabber ..."
echo -n " besoin de pidgin (y|N) ?"
read a
if [ "$a" = "y" ] || [ "$a" = "Y" ]; then
    apt-get install pidgin
    echo " pidgin installé ..."
    echo -n "presser Enter pour continuer"
    read anykey
fi
echo ""
echo " fin de la post-install."
echo -n " presser Enter pour quitter."
read anykey
exit 0
