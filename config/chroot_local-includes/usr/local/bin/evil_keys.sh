#! /bin/bash
# livarp_0.3.9 evil keybinds & shortcuts
########################################

## text colors ---------------------------------------------------------
red='\e[0;31m'
blue='\e[0;34m'
cyan='\e[0;36m'
green='\e[0;32m'
yellow='\e[0;33m'
## No Color
NC='\e[0m'
## background colors ---------------------------------------------------
RED='\e[41m'
BLUE='\e[44m'
CYAN='\e[46m'
GREEN='\e[42m'
YELLOW='\e[43m'
## output --------------------------------------------------------------
echo ""
echo -e "     ${green}raccourcis clavier evilwm-livarp 0.3.9$NC"
echo ""
echo " touche de modification par défaut : Mod = Ctrl + Alt"
echo ""
echo -e " ${RED} quitter evilwm     $NC Mod + Backspace"
echo -e " ${RED} quitter livarp     $NC Ctrl + Shift + Alt + q"
echo -e " ${RED} fermer le client   $NC Mod + echap"
echo ""
echo -e " ${GREEN} ouvrir un terminal   $NC Mod + Return"
echo ""
echo -e " ${BLUE} déplacer le client         $NC Mod + h/j/k/l ou Alt+B1"
echo -e " ${BLUE} positionner le client      $NC Mod + y/u/b/n"
echo -e " ${BLUE} redimensionner le client   $NC Mod + Shift + h/j/k/l ou Alt+B2"
echo -e " ${BLUE} passer le client derrière  $NC Mod + insert ou Alt+B3"
echo -e " ${BLUE} (dé)maximiser              $NC Mod + x"
echo -e " ${BLUE} (dé)maximiser en vertical  $NC Mod + ="
echo -e " ${BLUE} infos sur le client        $NC Mod + i"
echo -e " ${BLUE} client toujours visible    $NC Mod + f"
echo ""
echo -e " ${CYAN} tag préc/suivant     $NC Mod + Gauche/Droite"
echo -e " ${CYAN} client préc/suivant  $NC Alt + Tab"
echo -e " ${CYAN} afficher le tag 'n'  $NC Mod + [1..n]"
echo ""
echo ""
echo -e " ${green} manuel complet d'evilwm avec [man evilwm]"
echo -en " ${green} presser Enter pour quitter..."
read anykey
exit 0
