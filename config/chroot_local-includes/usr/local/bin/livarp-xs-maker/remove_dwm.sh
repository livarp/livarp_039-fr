#!/bin/bash
# livarp-xs maker
# dwm section

# this script is not available in live-session
# ----------------------------------------------------------------------
if [ -d /home/user ]; then
    zenity --info --text "ce script n'est pas disponible en session live" &
    exit
fi

# are you root ?
# ----------------------------------------------------------------------
if [ "`whoami`" != "root" ]; then
    zenity --info --text "ce script a besoin des droits administrateurs
    lancez le en tant que root" &
    exit
fi

# dwm stuff
# ----------------------------------------------------------------------
echo ""
echo " ---élimination de la session dwm---------------------------------"
echo ""
echo " élimination des fichiers système."
sleep 2s
rm /usr/local/bin/dwm
rm /usr/local/bin/dwm_keys.sh
rm /usr/share/applications/dwm.desktop
rm /usr/share/man/man1/dwm.1
echo ""
echo " élimination du script de lancement et du conky."
sleep 2s
rm $HOME/bin/start/dwm_start.sh
rm $HOME/.conky/.conkyrc_dwm
echo ""
echo " session dwm éliminée "
sleep 2s

#eof--------------------------------------------------------------------
