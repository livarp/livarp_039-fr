#!/bin/bash
# livarp-xs maker
# pekwm section

# this script is not available in live-session
# ----------------------------------------------------------------------
if [ -d /home/user ]; then
    zenity --info --text "ce script n'est pas disponible en session live" &
    exit
fi

# are you root ?
# ----------------------------------------------------------------------
if [ "`whoami`" != "root" ]; then
    zenity --info --text "ce script a besoin des droits administrateurs
    lancez le en tant que root" &
    exit
fi

# pekwm stuff
# ----------------------------------------------------------------------
echo ""
echo " ---élimination de la session pekwm-------------------------------"
echo ""
echo " élimination des fichiers système."
sleep 2s
apt-get autoremove -y --force-yes pekwm
rm -R -f /usr/share/pekwm
rm /usr/local/bin/pekwm_keys.sh
rm -R -f /etc/skel/.pekwm
echo ""
echo " élimination du script de lancement,"
echo " de la config et du conky."
sleep 2s
rm $HOME/bin/start/pekwm_start.sh
rm -R -f $HOME/.pekwm
rm $HOME/.conky/.conkyrc_pekwm
rm $HOME/.conky/.pek_rings.lua
echo ""
echo " session pekwm éliminée "
sleep 2s

#eof--------------------------------------------------------------------
