#!/bin/bash
# livarp-xs maker
# compiz section

# this script is not available in live-session
# ----------------------------------------------------------------------
if [ -d /home/user ]; then
    zenity --info --text "ce script n'est pas disponible en session live" &
    exit
fi

# are you root ?
# ----------------------------------------------------------------------
if [ "`whoami`" != "root" ]; then
    zenity --info --text "ce script a besoin des droits administrateurs
    lancez le en tant que root" &
    exit
fi

# compiz stuff
# ----------------------------------------------------------------------
echo ""
echo " ---élimination de la session compiz------------------------------"
echo ""
echo " élimination des fichiers système."
sleep 2s
apt-get autoremove -y --force-yes compiz compiz-fusion-plugins-main compiz-fusion-plugins-extra compizconfig-settings-manager libdecoration0
echo ""
echo " élimination des fichiers emerald."
sleep 2s
rm /usr/bin/emerald
rm /usr/bin/emerald-theme-manager
rm -R -f /usr/lib/emerald
rm /usr/lib/libemeraldengine.so.0
rm /usr/lib/libemeraldengine.so.0.0.0
rm /usr/lib/mime/packages/emerald
rm -R -f /usr/share/emerald
rm -R -f /usr/share/doc/emerald
rm -R -f /usr/share/doc/libemeraldengine0
rm /usr/share/man/man1/emerald-theme-manager.1.gz
rm /usr/share/man/man1/emerald.1.gz
rm /usr/share/mime-info/emerald.mime
echo ""
echo " élimination du script de lancement,"
echo " de la config et du conky."
sleep 2s
rm $HOME/bin/start/compiz_start.sh
rm $HOME/.conky/.conkyrc_compiz
rm -R -f $HOME/.config/compiz/compizconfig
rm -R -f $HOME/.emerald
rm -R -f /etc/skel/.emerald
rm -R -f /etc/skel/.config/compiz/compizconfig
echo ""
echo " session compiz éliminée "
sleep 2s

#eof--------------------------------------------------------------------
