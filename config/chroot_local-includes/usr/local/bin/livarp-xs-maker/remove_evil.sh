#!/bin/bash
# livarp-xs maker
# evilwm section

# this script is not available in live-session
# ----------------------------------------------------------------------
if [ -d /home/user ]; then
    zenity --info --text "ce script n'est pas disponible en session live" &
    exit
fi

# are you root ?
# ----------------------------------------------------------------------
if [ "`whoami`" != "root" ]; then
    zenity --info --text "ce script a besoin des droits administrateurs
    lancez le en tant que root" &
    exit
fi

# evil stuff
# ----------------------------------------------------------------------
echo ""
echo " ---élimination de la session evilwm------------------------------"
echo ""
echo " élimination des fichiers système."
sleep 2s
apt-get autoremove -y --force-yes evilwm
rm /usr/local/bin/evil_keys.sh
rm /usr/share/applications/evilwm.desktop
rm /usr/share/man/man1/evilwm.1
echo ""
echo " élimination du script de lancement et du conky."
sleep 2s
rm $HOME/bin/start/evil_start.sh
rm $HOME/.conky/.conkyrc_evil
echo ""
echo " session evilwm éliminée "
sleep 2s

#eof--------------------------------------------------------------------
