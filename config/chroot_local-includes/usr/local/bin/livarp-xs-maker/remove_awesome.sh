#!/bin/bash
# livarp-xs maker
# awesome section

# this script is not available in live-session
# ----------------------------------------------------------------------
if [ -d /home/user ]; then
    zenity --info --text "ce script n'est pas disponible en session live" &
    exit
fi

# are you root ?
# ----------------------------------------------------------------------
if [ "`whoami`" != "root" ]; then
    zenity --info --text "ce script a besoin des droits administrateurs
    lancez le en tant que root" &
    exit
fi

# awesome stuff
# ----------------------------------------------------------------------
echo ""
echo " ---élimination de la session awesome-----------------------------"
echo ""
echo " élimination des fichiers système."
sleep 2s
apt-get autoremove -y --force-yes awesome awesome-extra
echo ""
echo " élimination du script de lancement,"
echo " de la config et du conky."
sleep 2s
rm $HOME/bin/start/awesome_start.sh
rm $HOME/bin/awesome_calendar.sh
rm $HOME/.conky/.conkyrc_awesome
rm -R -f $HOME/.config/awesome
rm -R -f /etc/skel/.config/awesome
echo ""
echo " session awesome éliminée "
sleep 2s

#eof--------------------------------------------------------------------
