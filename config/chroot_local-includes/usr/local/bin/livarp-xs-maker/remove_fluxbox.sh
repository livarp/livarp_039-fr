#!/bin/bash
# livarp-xs maker
# fluxbox section

# this script is not available in live-session
# ----------------------------------------------------------------------
if [ -d /home/user ]; then
    zenity --info --text "ce script n'est pas disponible en session live" &
    exit
fi

# are you root ?
# ----------------------------------------------------------------------
if [ "`whoami`" != "root" ]; then
    zenity --info --text "ce script a besoin des droits administrateurs
    lancez le en tant que root" &
    exit
fi

# fluxbox stuff
# ----------------------------------------------------------------------
echo ""
echo " ---élimination de la session fluxbox-----------------------------"
echo ""
echo " élimination des fichiers système."
sleep 2s
apt-get autoremove -y --force-yes fluxbox
rm -R -f /usr/share/fluxbox
rm -R -f /etc/skel/.fluxbox
echo ""
echo " élimination du script de lancement,"
echo " de la config et du conky."
sleep 2s
rm $HOME/bin/start/fluxbox_start.sh
rm -R -f $HOME/.fluxbox
rm $HOME/.conky/.conkyrc_fluxbox
echo ""
echo " session fluxbox éliminée "
sleep 2s

#eof--------------------------------------------------------------------
