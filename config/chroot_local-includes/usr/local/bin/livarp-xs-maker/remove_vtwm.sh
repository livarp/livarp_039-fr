#!/bin/bash
# livarp-xs maker
# vtwm section

# this script is not available in live-session
# ----------------------------------------------------------------------
if [ -d /home/user ]; then
    zenity --info --text "ce script n'est pas disponible en session live" &
    exit
fi

# are you root ?
# ----------------------------------------------------------------------
if [ "`whoami`" != "root" ]; then
    zenity --info --text "ce script a besoin des droits administrateurs
    lancez le en tant que root" &
    exit
fi

# vtwm stuff
# ----------------------------------------------------------------------
echo ""
echo " ---élimination de la session vtwm--------------------------------"
echo ""
echo " élimination des fichiers système."
sleep 2s
apt-get autoremove -y --force-yes vtwm
rm /usr/local/bin/vtwm_keys.sh
rm -R -f /etc/skel/.vtwmrc
echo ""
echo " élimination du script de lancement,"
echo " de la config et du conky."
sleep 2s
rm $HOME/bin/start/vtwm_start.sh
rm $HOME/.vtwmrc
rm $HOME/.conky/.conkyrc_vtwm
echo ""
echo " session vtwm éliminée "
sleep 2s

#eof--------------------------------------------------------------------
