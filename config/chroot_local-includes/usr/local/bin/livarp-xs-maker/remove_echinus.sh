#!/bin/bash
# livarp-xs maker
# echinus section

# this script is not available in live-session
# ----------------------------------------------------------------------
if [ -d /home/user ]; then
    zenity --info --text "ce script n'est pas disponible en session live" &
    exit
fi

# are you root ?
# ----------------------------------------------------------------------
if [ "`whoami`" != "root" ]; then
    zenity --info --text "ce script a besoin des droits administrateurs
    lancez le en tant que root" &
    exit
fi

# echinus stuff
# ----------------------------------------------------------------------
echo ""
echo " ---élimination de la session echinus-----------------------------"
echo ""
echo " élimination des fichiers système."
sleep 2s
rm /usr/local/bin/echinus
rm /usr/local/bin/echinus_keys.sh
rm -R -f /usr/share/doc/echinus
rm /usr/share/man/man1/echinus.1
rm -R -f /etc/skel/.echinus
echo ""
echo " élimination du script de lancement,"
echo " de dzenbar et du conky."
sleep 2s
rm $HOME/bin/start/echinus_start.sh
rm $HOME/bin/dzenbar.sh
rm $HOME/bin/dzenbar_reload.sh
rm $HOME/.conky/.conkyrc_dzen
echo ""
echo " session echinus éliminée "
sleep 2s

#eof--------------------------------------------------------------------
