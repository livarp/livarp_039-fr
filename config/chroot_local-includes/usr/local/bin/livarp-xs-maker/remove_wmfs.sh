#!/bin/bash
# livarp-xs maker
# wmfs section

# this script is not available in live-session
# ----------------------------------------------------------------------
if [ -d /home/user ]; then
    zenity --info --text "ce script n'est pas disponible en session live" &
    exit
fi

# are you root ?
# ----------------------------------------------------------------------
if [ "`whoami`" != "root" ]; then
    zenity --info --text "ce script a besoin des droits administrateurs
    lancez le en tant que root" &
    exit
fixit
fi

# wmfs stuff
# ----------------------------------------------------------------------
echo ""
echo " ---élimination de la session wmfs--------------------------------"
echo ""
echo " élimination des fichiers système."
sleep 2s
rm /usr/local/bin/wmfs
rm /usr/local/bin/shortcuts_help.sh
rm /usr/local/bin/keybind_help.sh
rm /usr/share/applications/wmfs.desktop
rm /usr/share/man/man1/wmfs.1
rm -R -f /etc/xdg/wmfs
rm -R -f /etc/skel/.config/wmfs
echo ""
echo " élimination du script de lancement,"
echo " de la config et du conky."
sleep 2s
rm $HOME/bin/start/wmfs_start.sh
rm -R -f $HOME/.config/wmfs
rm $HOME/.conky/.conkyrc_wmfs
echo ""
echo " session wmfs éliminée "
sleep 2s

#eof--------------------------------------------------------------------
