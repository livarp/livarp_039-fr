#! /bin/bash
# livarp_0.3.9 dwm keybinds & shortcuts
#######################################

## text colors ---------------------------------------------------------
red='\e[0;31m'
blue='\e[0;34m'
cyan='\e[0;36m'
green='\e[0;32m'
yellow='\e[0;33m'
## No Color
NC='\e[0m'
## background colors ---------------------------------------------------
RED='\e[41m'
BLUE='\e[44m'
CYAN='\e[46m'
GREEN='\e[42m'
YELLOW='\e[43m'
## output --------------------------------------------------------------
echo ""
echo -e "     ${green}raccourcis clavier dwm-livarp 0.3.9$NC"
echo ""
echo -e " ${RED} relancer dwm       $NC Shift + Alt + q"
echo -e " ${RED} quitter dwm        $NC Ctrl + Alt + Backspace"
echo -e " ${RED} quitter livarp     $NC Ctrl + Shift + Alt + q"
echo -e " ${RED} fermer le client   $NC Alt + q"
echo ""
echo -e " ${BLUE} lancer dmenu       $NC Alt + p"
echo -e " ${BLUE} scratchpad         $NC F12"
echo ""
echo -e " ${GREEN} ouvrir un terminal  $NC Ctrl + Return"
echo -e " ${GREEN} ouvrir rox-filer    $NC Alt + Shift + r"
echo -e " ${GREEN} ouvrir ranger       $NC Alt + r"
echo -e " ${GREEN} ouvrir luakit       $NC Alt + w"
echo -e " ${GREEN} ouvrir firefox      $NC Alt + Shift + w"
echo -e " ${GREEN} ouvrir vim editor   $NC Alt + e"
echo -e " ${GREEN} ouvrir geany        $NC Alt + Shift + e"
echo -e " ${GREEN} ouvrir weechat      $NC Alt + x"
echo -e " ${GREEN} ouvrir music player $NC Alt + z"
echo -e " ${GREEN} controler le volume $NC Alt + v"
echo ""
echo -e " ${BLUE} agencement tile        $NC Alt + t"
echo -e " ${BLUE} agencement bottomstack $NC Alt + s"
echo -e " ${BLUE} agencement monocle     $NC Alt + m"
echo -e " ${BLUE} agencement floating    $NC Alt + f"
echo -e " ${BLUE} remplacer l'agencement $NC Alt + espace"
echo -e " ${BLUE} changer l'agencement   $NC Super + espace"
echo ""
echo -e " ${CYAN} client prec/suivant    $NC Alt + k/j"
echo -e " ${CYAN} tag prec/suivant       $NC Ctrl + Gauche/Droite"
echo -e " ${CYAN} écran prec/suivant     $NC Ctrl + Bas/Haut"
echo ""
echo -e " ${CYAN} envoi sur l'écran prec/suivant $NC Ctrl + Shift + Bas/Haut"
echo -e " ${CYAN} envoi sur le tag 'n'           $NC Alt + Shift + [1..n]"
echo -e " ${CYAN} afficher le tag 'n'            $NC Alt + [1..n]"
echo -e " ${CYAN} libérer le client              $NC Alt + Shift + espace"
echo ""
echo ""
echo -e " ${green} manuel complet de dwm >> [man dwm]"
echo -en " ${green} presser Enter pour quitter..."
read anykey
exit 0
