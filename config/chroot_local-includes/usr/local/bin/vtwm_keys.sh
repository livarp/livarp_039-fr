#! /bin/bash
# livarp_0.3.9 vtwm keybinds & shortcuts
########################################

## text colors ---------------------------------------------------------
red='\e[0;31m'
blue='\e[0;34m'
cyan='\e[0;36m'
green='\e[0;32m'
yellow='\e[0;33m'
## No Color
NC='\e[0m'
## background colors ---------------------------------------------------
RED='\e[41m'
BLUE='\e[44m'
CYAN='\e[46m'
GREEN='\e[42m'
YELLOW='\e[43m'
## output --------------------------------------------------------------
echo ""
echo -e "     ${green}raccourcis clavier vtwm-livarp 0.3.9$NC"
echo ""
echo -e " ${RED} relancer vtwm     $NC Shift + Alt + r"
echo -e " ${RED} quitter vtwm      $NC Ctrl + Shift + q"
echo -e " ${RED} quitter livarp    $NC Ctrl + Shift + Alt + q"
echo -e " ${RED} quitter le client $NC Super + q"
echo -e " ${RED} tuer le client    $NC Super + x"
echo ""
echo -e " ${BLUE} lancer dmenu        $NC Super + d"
echo ""
echo -e " ${GREEN} ouvrir un terminal   $NC Ctrl + Return"
echo -e " ${GREEN} ouvrir rox-filer     $NC Super + Shift + r"
echo -e " ${GREEN} ouvrir ranger        $NC Super + r"
echo -e " ${GREEN} ouvrir firefox       $NC Super + w"
echo -e " ${GREEN} contrôleur de volume $NC Super + v"
echo ""
echo -e " ${CYAN} client préc/suivant  $NC Alt + Tab"
echo -e " ${CYAN} bureau préc/suivant  $NC Ctrl + Left/Right"
echo ""
echo -e " ${CYAN} (dé)maximiser le client  $NC Super + m"
echo -e " ${CYAN} (dé)minimiser le client  $NC Super + i"
echo ""
echo -e " ${green} manuel complet avec [man vtwm]"
echo -en " ${green} presser Enter pour quitter..."
read anykey
exit 0
